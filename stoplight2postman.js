/**
This function converts a list of Stoplight environments into a single .postman environment file
inputEnvs must be an array of JSON strings
*/

function convertStoplightEnv2Postman(inputEnvs, inputName) {

    var postmanEnvironment = {
	"version": 1,
	"collections": [],
	"environments": []
	};

	for(var i = 0; i < inputEnvs.length; i++){
		try {
			var stoplightEnv = JSON.parse(inputEnvs[i]);
			
			for (const [nameEnv, values] of Object.entries(stoplightEnv.environments)) {
				var env = {id: "guid",name: inputName + "_" + nameEnv, values: []};
				console.log(`${nameEnv}: ${values}`);
				for (const [key, value] of Object.entries(values)) {
					env.values.push({
						key: key,
						value: value,
						enabled: "true"
					})
					console.log(`${key}: ${value}`);
				}
				postmanEnvironment.environments.push(env);
			}
			console.log("ENV : " + env);
		} catch (error) {
			console.log("This input is not JSON well-formed.");
		}
		
	}
	console.log("RESULT : " + postmanEnvironment);
    return postmanEnvironment;
}